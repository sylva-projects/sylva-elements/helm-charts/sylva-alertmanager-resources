# sylva-alertmanager-webhook-jiralert

Generates the jiralert config file

More info on possible values:

https://github.com/prometheus-community/helm-charts/blob/main/charts/jiralert/values.yaml

specifically the `config` key
