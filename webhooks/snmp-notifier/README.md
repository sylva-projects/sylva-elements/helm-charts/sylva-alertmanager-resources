# sylva-alertmanager-webhook-snmp-notifier

Generates the snmp-notifier template files

More info on possible values:

https://github.com/prometheus-community/helm-charts/blob/main/charts/alertmanager-snmp-notifier/values.yaml

specifically the `config` key
